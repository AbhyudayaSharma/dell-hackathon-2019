# `ArrayIndexOutOfBounds` - Dynamic UI Creator

## Project Overview

This application allows the users to create a UI according to the components 
they value the most. The users will be able to decide which parts of the UI are
the most important and arrange them on their web pages. This gives a lot of
flexibility to both the users and the developers. The developers would then not
have to think how to place the UI and the user can decide what is best for them.

## Solution Description

### Architecture Diagram

![Architecture Diagram](./Documents/FlowDiagram.png)

### Technologies Used

The application uses React.JS for creating the UI as reusable components. The
data visible to the users is made up of these components.

### Running the Code

To run the application, the user needs to have Node.js version >= 12.0 with
`npm` installed on his or her machine. To run locally, the following terminal
commands are to be used:

```bash
npm install
npm start
```

To compile a production build, run
```bash
npm build
```

## Team Members

* Abhyudaya Sharma <sharmaabhyudaya@gmail.com>
* Jackson Jose <jacksonjose4999@gmail.com>
* Kabir Kanha Arora <kabirkanha@gmail.com>
* Mohit S <ms207@snu.edu.in>
