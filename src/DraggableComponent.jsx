import React from 'react';
import DeleteButton from "./DeleteButton";

class DraggableComponent extends React.Component {
  deleteButton;
  onDelete;

  constructor(props) {
    super(props);
    this.deleteButton = <DeleteButton key={2} onClick={() => this.eventHandler()}/>;
  }

  render() {
    return (
      <div className='Container-component' key={0}>
        {<this.props.component key={1}/>}
        {this.deleteButton}
      </div>
    )
  }

  eventHandler() {
    this.props.onDelete();
  }
}


export default DraggableComponent;
