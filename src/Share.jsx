import React from 'react';
import './Share.css'
import salesArrow from './Images/salesArrow.png';
import downArrow from './Images/downArrow.png';
import upArrow from './Images/upArrow.png';

class Share extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arrowImage: downArrow,
      percentage: 0.0,
      price: 0,
    }
  }

  render() {
    return (
      <div className="Share-rounded">
        <p className="Share-boxTitle"><img src={salesArrow} className="Share-price" alt="Price Icon"/>
          <br/>Share Price</p>
        <p>NYSE: DELL</p>
        <p className="Share-display">{this.state.price}</p>
        <p style={{display: 'inline'}}><img className="Share-arrow" src={this.state.arrowImage} alt="Arrow"/>
        </p>
        <p className="Share-info">{this.state.percentage}</p>
      </div>
    );
  }

  componentDidMount() {
    this.setState({
      arrowImage: this.state.arrowImage,
      intervalVar: setInterval(Share.myFunction, 1500, this),
    })
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalVar);
  }

  static myFunction(inst) {
    let x = Share.generateRandomInteger(53, 58);
    x = x.toFixed(2);
    let str1 = "$";
    let str2 = x.toString();
    let price = str1.concat(str2);

    let str3 = "(";
    let y = Share.generateRandomInteger(0.00, 2.00);
    y = y.toFixed(2);
    let str4 = y.toString();
    let str5 = "%)";
    let z = Math.random();
    let str_sign;
    let image;
    if ((z > 0.5)) {
      str_sign = "+";
      image = upArrow;
    } else {
      str_sign = "-";
      image = downArrow;
    }

    let percentage = str_sign.concat(str3, str4, str5);
    const newState = {
      arrowImage: image,
      percentage: percentage,
      price: price,
    };

    if (inst.hasOwnProperty('intervalVar')) {
      newState.intervalVar = inst.intervalVar;
    }

    inst.setState(newState);
  }

  static generateRandomInteger(min, max) {
    return min + Math.random() * (max + 1 - min)
  }
}

export default Share;
