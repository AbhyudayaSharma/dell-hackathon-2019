// Step 1 - Include react
import React from 'react';
// Step 2 - Include the react-fusioncharts component
import ReactFC from 'react-fusioncharts';
// Step 3 - Include the fusioncharts library
import FusionCharts from 'fusioncharts';
// Step 4 - Include the chart type
import Column2D from 'fusioncharts/fusioncharts.charts';
// Step 5 - Include the theme as fusion
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

// Step 6 - Adding the chart and theme as dependency to the core fusioncharts
ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

// Step 7 - Creating the JSON object to store the chart configurations
const chartConfigs = {
  type: 'column2d',// The chart type
  width: '400', // Width of the chart
  height: '250', // Height of the chart
  dataFormat: 'json', // Data type
  dataSource: {
    // Chart Configuration
    "chart": {
      "caption": "Dell Sales",
      "subCaption": "XPS 13 Units in millions",
      "xAxisName": "Country",
      "yAxisName": "Units Sold (MMbbl)",
      "numberSuffix": "K",
      "theme": "fusion",
    },
    // Chart Data
    "data": [{
      "label": "Venezuela",
      "value": "290"
    }, {
      "label": "Saudi",
      "value": "260"
    }, {
      "label": "Canada",
      "value": "180"
    }, {
      "label": "Iran",
      "value": "140"
    }, {
      "label": "Russia",
      "value": "115"
    }, {
      "label": "UAE",
      "value": "100"
    }, {
      "label": "US",
      "value": "30"
    }, {
      "label": "China",
      "value": "30"
    }]
  }
};

// Step 9 - Creating the DOM element to pass the react-fusioncharts component
class BarChart extends React.Component {
  render() {
    return (
      <ReactFC {...chartConfigs}/>
    );
  }
}

export default BarChart;
