import React, { Component } from 'react';
import "./Table.css";

class Table extends Component {
    constructor(props) {
        super(props); //since we are extending class Table so we have to use super in order to override Component class constructor
        this.state = { //state is by default an object
            students: [
                { id: 1, name: 'Jackson', age: 20, email: 'jackthegreat@gmail.com' },
                { id: 2, name: 'Abhudaya', age: 20, email: 'abhudaya@email.com' },
                { id: 3, name: 'Kabir Khan', age: 20, email: 'Kabir@email.com' },
                { id: 4, name: 'Mohith', age: 20, email: 'mohith@email.com' }
            ]
        };
    }

    renderTableHeader() {
        let header = Object.keys(this.state.students[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableData() {
        return this.state.students.map((student, index) => {
            const { id, name, age, email } = student
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{age}</td>
                    <td>{email}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <table className='students'>
                    <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Table;