import React from 'react';
import './Navbar.css';

class NavBar extends React.Component {

  render() {
    return (
      <div className="navbar">
        <h1 className="navbar-h1">
          Statistics Dashboard
        </h1>
      </div>
    );
  }
}

export default NavBar;
