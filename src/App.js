import React from 'react';
import './App.css';

import NavBar from "./NavBar";
import Container from "./Container";

function App() {
  return (
    <div className="App-body">
      <NavBar/>
      <Container/>
    </div>
  );
}

export default App;
