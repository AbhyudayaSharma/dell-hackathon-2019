import React from 'react';
import './ComponentListEntry.css';

class ComponentListEntry extends React.Component {
  onClick;
  componentName;

  render() {
    return (
      <div className='ComponentListEntry-entry' onClick={this.props.onClick}>
        {this.props.componentName}
      </div>
    );
  }
}

export default ComponentListEntry;
