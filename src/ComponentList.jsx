import React from 'react';
import './NavBar';
import './Share';
import ComponentListEntry from "./ComponentListEntry";
import Share from "./Share";
import LineChart from "./LineChart";
import PieChart from "./PieChart";
import BarChart from "./BarChart";
import LargeLabel from "./Label";
import TableView from "./TableView"
import TodoListApp from "./TodoList";

class ComponentList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      components: [
        <ComponentListEntry componentName='Shares' onClick={() => this.props.onClick(Share)} key='0'/>,
        <ComponentListEntry componentName='Line Chart' onClick={() => this.props.onClick(LineChart)} key='1'/>,
        <ComponentListEntry componentName='Label' onClick={() => this.props.onClick(LargeLabel)} key='2'/>,
        <ComponentListEntry componentName='Pie Chart' onClick={() => this.props.onClick(PieChart)} key='4'/>,
        <ComponentListEntry componentName='Bar Chart' onClick={() => this.props.onClick(BarChart)} key='5'/>,
        <ComponentListEntry componentName='Table View' onClick={() => this.props.onClick(TableView)} key='6'/>,
        // <ComponentListEntry componentName='Todo List' onClick={() => this.props.onClick(TodoListApp)} key='7'/>,
      ],
    };
  }

  render() {
    return (
      <div>
        {this.state.components}
      </div>
    );
  }
}

export default ComponentList;
