import React from 'react';
import ReactFC from 'react-fusioncharts';

const chartConfigs = {
  type: 'pie2d',// The chart type
  width: '400', // Width of the chart
  height: '250', // Height of the chart
  dataFormat: 'json', // Data type
  dataSource: {
    // Chart Configuration
    "chart": {
      "showPercentValues": "1",
      "showPercentInTooltip": "0"
    },
    // Chart Data
    "data": [{
      "label": "Food",
      "value": "285040"
    },
      {
        "label": "Apparels",
        "value": "65",
      },
      {
        "label": "Electronics",
        "value": "105070"
      },
      {
        "label": "Household",
        "value": "49100"
      }
    ]
  }
};

// Step 9 - Creating the DOM element to pass the react-fusioncharts component
class PieChart extends React.Component {

  render() {
    return (
      <ReactFC {...chartConfigs}/>
    );
  }
}

export default PieChart;
