import React from 'react';
import './Container.css';

class DeleteButton extends React.Component {
  render() {
    return (
      <div className='Container-delete-button' onClick={this.props.onClick}>X</div>
    );
  }
}

export default DeleteButton;
