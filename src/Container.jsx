import React from 'react';
import './Container.css';
import ComponentList from './ComponentList';
import DraggableComponent from "./DraggableComponent";

class Container extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      components: [],
    }
  }

  componentAdded(Comp) {
    const newComponents = this.state.components.slice();
    newComponents.push(Comp);
    this.setState({
      components: newComponents,
    });
  }

  componentDeleted(index) {
    const newComponents = this.state.components.slice();
    newComponents.splice(index, 1);
    this.setState({
      components: newComponents,
    });
  }

  render() {
    return (
      <div className="container">
        <div className="container-left">
          <ComponentList onClick={Comp => this.componentAdded(Comp)}/>
        </div>
        <div className="container-right">
          {this.state.components.map((Comp, index) => (
            <DraggableComponent key={index} component={Comp} onDelete={() => this.componentDeleted(index)}/>
          ))}
        </div>
      </div>
    );
  }
}

export default Container;
